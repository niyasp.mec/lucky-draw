import { Component, ViewChild } from '@angular/core';
import { NgxWheelComponent, TextAlignment, TextOrientation } from 'ngx-wheel'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  @ViewChild(NgxWheelComponent, { static: false }) wheel;

  seed = ["Fasal","Fawaz","Ayisha","Aslam","Irshad","Shafeek","Sarfaz","Nadeed","Iqbal","Noorul Hassan","Lukman","Arshad"]; // [...Array(12).keys()]
  idToLandOn: any;
  items: any[];
  textOrientation: TextOrientation = TextOrientation.HORIZONTAL
  textAlignment: TextAlignment = TextAlignment.OUTER

  ngOnInit(){
    this.idToLandOn =""
    this.idToLandOn = this.seed[Math.floor(Math.random()* this.seed.length)];
    const colors = ['#FF0000', '#000000']
    this.items = this.seed.map((value) => ({
      fillStyle: colors[0],
      text: ` ${value}`,
      id: value,
      textFillStyle: 'white',
      textFontSize: '16'
    }))
  }
  reset() {
    //this.idToLandOn =""
    this.wheel.reset()
  }
  before() {
    alert('Your wheel is about to spin')
  }

//   async spin() {

//     const random = Math.floor(Math.random() * this.seed.length);
// console.log(random, this.seed[random]);
//     this.idToLandOn = this.seed[random]
//     //await new Promise(resolve => setTimeout(resolve, 0));
//     this.wheel.spin()
//   }

  after() {
    alert('Congratulations '+this.idToLandOn+'.You have won the Lucky Draw.....')
  }
}
